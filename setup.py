#!/usr/bin/env python

import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
doclink = """
Documentation
-------------

The full documentation is at http://flyngo.rtfd.org."""
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='flyngo',
    version='0.1.1',
    description='Launch CS:GO servers for your tournaments on the fly using a baremetal cloud provider.',
    long_description=readme + '\n\n' + doclink + '\n\n' + history,
    author='Ryan Lahfa',
    author_email='ryan@lahfa.xyz',
    url='https://gitlab.com/sgl-eng/flyngo',
    packages=[
        'flyngo',
    ],
    package_dir={'flyngo': 'flyngo'},
    include_package_data=True,
    install_requires=[
        'asyncssh',
        'scaleway-sdk',
        'haikunator',
        'jinja2<2.9',
        'pyyaml',
        'packet-python',
        'ansible'
    ],
    license='MIT',
    zip_safe=False,
    keywords='flyngo',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
)
