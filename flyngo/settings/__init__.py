driver = None
driver_opts = {}

provisionner = None
provisionner_opts = {}

artifact_handler = 'flyngo.artifacts.SimpleArtifactHandler'
artifact_opts = {}

storage = 'flyngo.storage.LocalStorage'
storage_opts = {
    'target_folder': 'flyngo_storage/'
}

game_opts = {}

__initialized = False
__verified = False
