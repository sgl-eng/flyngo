============
Installation
============

At the command line either via easy_install or pip::

    $ easy_install flyngo
    $ pip install flyngo

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv flyngo
    $ pip install flyngo

