import asyncio
from collections import deque
from typing import List, Any


class UnknownEventError(RuntimeError):
    pass


def populate_event_types() -> List[Any]:
    return []


class SourceLogProtocol(asyncio.Protocol):
    EVENTS_TYPE = populate_event_types()

    def __init__(self, skip_unknown: bool=True):
        self._transport = None
        self.events = deque()
        self.skip_unknown = skip_unknown

        self.current_buffer = bytes()
        self.on_server_closed = asyncio.Future()

    def connection_made(self, transport):
        self._transport = transport

    def line_received(self, line):
        line = line.strip()
        for re, cls in self.EVENTS_TYPE:
            match = re.match(line)
            if match:
                event = cls.from_re_match(match)
                self.events.append(event)
                return

        if not self.skip_unknown:
            raise UnknownEventError('Could not parse event: {}'.format(line))

    def datagram_received(self, data, addr):
        # concatenate new data.
        self.current_buffer += data
        # split into buffers by \n delimiter.
        buffers = self.current_buffer.split('\n')
        # the new current buffer is the last buffer.
        self.current_buffer = buffers[:-1]
        # the rest is lines.
        for line in buffers[:-1]:
            self.line_received(line)

    def error_received(self, exc):
        pass

    def connection_lost(self, exc):
        self.on_server_closed.done()


def connection_requested():
    return SourceLogProtocol()


async def run_server():
    loop = asyncio.get_event_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        connection_requested,
        remote_addr=('0.0.0.0', 12360)
    )

    return transport, protocol
