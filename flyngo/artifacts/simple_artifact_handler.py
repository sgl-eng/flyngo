from .abstract_artifact_handler import AbstractArtifactHandler
from flyngo.servers import Server


class SimpleArtifactHandler(AbstractArtifactHandler):
    async def upload(self, server: Server) -> None:
        """
        Upload DEM files to the underlying storage.
        Assuming that tv_autorecord 1 is set on the CS:GO server.

        :param server: a server instance hosting a game
        :type server: Server
        :return: None
        :rtype: NoneType
        """

        root = await server.connect_as_root()
        dem_files = await self.search_for_dem_files(root)

        paths = await self.download_files(root, dem_files)
        await self.storage.batch_save(paths)
