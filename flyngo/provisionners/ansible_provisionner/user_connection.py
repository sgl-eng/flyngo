from flyngo.servers.ssh_connection import SSHConnection
from flyngo.servers.server import Server
from flyngo.utils.lgsm_account import LGSMAccount
from typing import Any, Dict


class UserConnection:
    def __init__(self,
                 instance_data: Dict[str, Any],
                 server: Server):
        self.builder = server.connect_as_root
        self._conn = None
        self.acc = None
        self.username = instance_data['username']

    async def boot(self):
        self._conn = await self.builder()
        self.acc = LGSMAccount(await self._conn.su(self.username))

    async def edit_hostname(self, hostname):
        await self.acc.replace_extra_config('hostname', hostname)

    async def edit_password(self, password):
        await self.acc.replace_extra_config('sv_password', password)

    async def edit_rcon_password(self, rcon_password):
        await self.acc.replace_extra_config('rcon_password', rcon_password)

    async def restart_server(self):
        await self.acc.restart()
