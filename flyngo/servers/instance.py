from typing import Optional
from flyngo.utils import generate_random_password
import asyncio
import importlib


class Instance:
    def __init__(self,
                 server,
                 address: str,
                 rcon_password: str,
                 password: str = None,
                 port: int = 27015,
                 sourcetv_port: Optional[int] = None,
                 hostname: Optional[str] = None):
        self.address = address
        self.hostname = hostname
        self.rcon_password = rcon_password
        self.password = password
        self.port = port
        self.sourcetv_port = sourcetv_port
        self.server = server
        self.extra = {}  # For provisionner-specific data.
        self._user_conn = None
        self._user_conn_module = None

    def __str__(self):
        return (
            '<Instance host: {}, '
            'RCon: {}, '
            'password: {}, '
            'port: {}, '
            'sourcetv port: {}, '
            'game hostname: {}, '
            'extra: {}'
        ).format(self.address,
                 self.rcon_password,
                 self.password,
                 self.port,
                 self.sourcetv_port,
                 self.hostname,
                 self.extra)

    def to_dict(self):
        return {
            'address': self.address,
            'hostname': self.hostname,
            'rcon_password': self.rcon_password,
            'password': self.password,
            'port': self.port,
            'sourcetv_port': self.sourcetv_port,
            'server_id': self.server.id,
            'extra': self.extra,
            'had_user_connection': self._user_conn is not False,
            'user_connection_module': self._user_conn_module
        }

    @classmethod
    def from_dict(cls, dict_representation, server) -> 'Instance':
        assert server.id == dict_representation['server_id'], 'Server IDs does not match!'
        instance = Instance(
            server,
            dict_representation['address'],
            dict_representation['rcon_password'],
            dict_representation['password'],
            dict_representation['port'],
            dict_representation['sourcetv_port'],
            dict_representation['hostname']
        )

        instance.extra = dict_representation['extra']
        if dict_representation['had_user_connection']:
            *mods, klass = dict_representation['user_connection_module'].split('.')
            mod = importlib.import_module('.'.join(mods))
            klass = getattr(mod, klass, None)
            if not klass:
                raise RuntimeError('Failed to recreate the user connection module.')
            instance.setup_connection(klass)

        return instance

    def supports_reusage(self):
        return bool(self._user_conn)

    async def edit_hostname(self, hostname):
        await self._user_conn.edit_hostname(hostname)
        self.hostname = hostname

    async def edit_password(self, password):
        await self._user_conn.edit_password(password)
        self.password = password

    async def edit_rcon_password(self, rcon_password):
        await self._user_conn.edit_rcon_password(rcon_password)
        self.rcon_password = rcon_password

    async def restart_server(self):
        await self._user_conn.restart_server()

    async def randomize_password(self):
        await self.edit_password(generate_random_password())

    async def randomize_rcon_password(self):
        await self.edit_rcon_password(generate_random_password())

    async def shape_instance(self, hostname: str,
                             *,
                             password: Optional[str] = None,
                             rcon_password: Optional[str] = None):
        if password is None:
            password = generate_random_password()
        if rcon_password is None:
            rcon_password = generate_random_password()

        await asyncio.gather(self.edit_hostname(hostname),
                             self.edit_password(password),
                             self.edit_rcon_password(rcon_password))

        await self.restart_server()

    def has_siblings(self):
        return len(self.server.instances) > 1

    def setup_connection(self, user_connection_class):
        self._user_conn_module = user_connection_class.__module__ + '.' + user_connection_class.__qualname__
        self._user_conn = user_connection_class(self.extra, self.server)

    async def boot_connection(self):
        await self._user_conn.boot()

    @property
    def steam_url(self):
        ip = self.address
        port = self.port
        password = self.password
        if not ip and not port:
            return None
        else:
            return 'steam://connect/{ip}:{port}/{password}'.format(ip=ip,
                                                                   port=port,
                                                                   password=password)
