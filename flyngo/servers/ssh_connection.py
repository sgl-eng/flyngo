import crypt
import asyncssh
from typing import Optional, Dict, Any


class SSHConnection:
    def __init__(self, client: asyncssh.SSHClientConnection, extra_params: Dict[str, Any]):
        self.c = client
        self._extra_params = extra_params
        self.machine_data = {}

    async def read_machine_data(self):
        """
        Populate the `machine_data` dict.
        :return: None
        :rtype: NoneType
        """
        result = await self.execute('cat /etc/issue')
        issue = result.stdout
        distribution, version = issue.split(' ')[:2]
        self.machine_data['distribution'] = distribution

        try:
            self.machine_data['distribution_version'] = int(version)
        except ValueError:
            pass

    async def execute(self, *args, **kwargs) -> asyncssh.SSHCompletedProcess:
        """
         Take the same arguments as paramiko.client.SSHClient.exec_command
        :return: stdin, stdout, stderr
        :rtype: (file, file, file)
        """
        kwargs['check'] = kwargs.get('check', True)
        return await self.c.run(*args, **kwargs)

    async def su(self, username,
                 copy_keys: bool = True,
                 password: Optional[str] = None) -> 'SSHConnection':
        """
        Return a new SSHConnection which wraps a SSHClientConnection
        which has reconnected using a new user.

        :param username: The new user account to su into
        :type username: str
        :param copy_keys: Copy authorized_keys
        :param password: (if don't copy keys) the password to use
        :type password: Optional[str]
        :return: a SSH connection wrapping a channel
        :rtype: SSHConnection
        """
        # Either, pass keys, either use password auth.
        assert copy_keys or (password is not None)

        # Pass keys.
        if copy_keys:
            await self.execute('mkdir -p /home/{}/.ssh/'.format(username))
            await self.execute('touch /home/{}/.ssh/authorized_keys'.format(username))
            await self.execute(
                'diff ~/.ssh/authorized_keys /home/{}/.ssh/authorized_keys > /tmp/patch.new'.format(username),
                check=False
            )
            await self.execute('patch -p1 -R /home/{}/.ssh/authorized_keys /tmp/patch.new'.format(username))
            await self.execute('chown {user}:{user} /home/{user}/.ssh/authorized_keys'.format(user=username))

        address, port = self.c.get_extra_info('peername')
        kwargs = {
            'port': port,
            'username': username
        }
        if not copy_keys:
            kwargs['password'] = password

        kwargs.update(self._extra_params)

        new_connection = await asyncssh.connect(address, **kwargs)

        return SSHConnection(new_connection, self._extra_params)

    async def add_user(self, username, password: Optional[str] = None) -> None:
        """
        Create a new user using `useradd` method.
        Create a home folder also.

        :param username: username for the new user
        :type username: str
        :param password: password for the new user
        :type password: str
        :return: None
        :rtype: NoneType
        """
        options = '-m -d /home/{0}'
        if password:
            crypted_password = crypt.crypt(password)
            options += ' -p {}'.format(crypted_password)

        command = 'useradd ' + options + ' {0}'
        await self.execute(command.format(username))

    async def engage_sftp_subsystem(self) -> asyncssh.SFTPClient:
        return await self.c.start_sftp_client()

    async def get_primary_ip(self, interface: str = None) -> str:
        initial_command = "ifconfig {}".format(interface if interface else '')

        command = "| grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*'"

        res = await self.execute(initial_command + command)
        ip = res.stdout.split(':')[-1].strip()
        return ip
