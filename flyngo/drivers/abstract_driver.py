from typing import Dict, Optional, Tuple, List
from flyngo.servers import Server
import asyncio

DriverOptions = Optional[Dict[str, str]]


class AbstractDriver:
    def __init__(self,
                 driver_opts: DriverOptions,
                 loop: asyncio.AbstractEventLoop = None):
        if driver_opts is None:
            self.driver_opts = []
        else:
            self.driver_opts = driver_opts

        if loop is None:
            self.loop = asyncio.get_event_loop()

    @staticmethod
    def supports_inspection() -> bool:
        """
        If the driver supports machine inspection at runtime.
        :return: True if supporting, False otherwise.
        :rtype: bool
        """
        return False

    @staticmethod
    def supports_capacity() -> bool:
        """
        If the driver supports capacity system at runtime.
        :return: True if supporting, False otherwise.
        :rtype: bool
        """
        return False

    @property
    def capacity_per_machine(self) -> int:
        """
        Capacity per machine, for multiple servers.
        :return: the capacity
        :rtype: int
        """
        return 1

    async def inspect(self) -> Tuple[List[Server], List[Server]]:
        """
        Inspect the current infrastructure for flyngo's controlled servers.

        May populate internal variables for further usage.

        :return: list of used flyngo servers and unused flyngo servers.
        :rtype: Tuple[List[Server], List[Server]]
        """
        raise NotImplementedError

    async def update_capacity(self, server: Server, new_capacity: int) -> None:
        """
        Update the capacity using the driver inner workings.

        :param server: the server to change the capacity
        :type server: Server
        :param new_capacity: the new value
        :type new_capacity: positive integer
        :return: None
        :rtype: NoneType
        """
        raise NotImplementedError

    def has_unused_servers(self) -> bool:
        """
        If the driver supports machine recycling,
        then, you can use this method to signal of unused machines
        so that future provisions might be faster (remove the cost of creating a new machine)

        :return: True if has unused servers, False otherwise.
        :rtype: bool
        """
        raise NotImplementedError

    def pick_a_free_server(self) -> Server:
        """
        If the driver supports machine recycling,
        return a machine ready to be recycled.

        :return: a free server
        :rtype: Server
        """
        raise NotImplementedError

    def recycle(self, server: Server) -> None:
        """
        If the driver supports machine recycling,
        Here is a good candidate to recycling.

        :param server: Server to recycle
        :type server: Server
        :return: None
        :rtype: NoneType
        """
        raise NotImplementedError

    async def destroy(self, server: Server) -> None:
        """
        Destroy the machine connected to this server.
        Free up resources and save money $$$ !

        :param server: Server to destroy
        :type server: Server
        :return: None
        :rtype: NoneType
        """
        raise NotImplementedError

    async def fire_up_a_machine(self) -> Server:
        """
        Fire up a new machine using the driver API.
        :return: (asynchronously) a new server.
        :rtype: Server
        """
        raise NotImplementedError
