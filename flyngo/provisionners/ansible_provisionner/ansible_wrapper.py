import asyncio
import json
import logging
import os
import uuid
from typing import Any, Dict

logger = logging.getLogger('flyngo.ansible_provisionner.wrapper')


class AnsibleWrapper:
    def __init__(self, playbook_path: str, private_key: str = None, loop: asyncio.AbstractEventLoop = None):
        self.loop = loop or asyncio.get_event_loop()
        self.private_key = private_key
        self.playbook_path = playbook_path

    async def run(self, host_list: str, group_variables: Dict[str, Any]):
        uuid_path = '/tmp/ansible_{}/'.format(uuid.uuid4())
        inventory_path = os.path.join(
            uuid_path,
            'inventory'
        )
        os.makedirs(uuid_path, exist_ok=True)
        with open(inventory_path, 'w') as f:
            f.write('[gameservers]\n')
            f.write(host_list + ' ansible_user=root ansible_ssh_extra_args="-o StrictHostKeyChecking=false"')

        definitive_group_vars = group_variables.copy()

        before = []
        if self.private_key:
            logger.debug('Using private key: {}'.format(self.private_key))
            before.append('--private-key={}'.format(self.private_key))

        process = await asyncio.create_subprocess_shell(
            'ansible-playbook {before} -i {inventory_path} -e \'{extra_vars}\' {playbook_path}'
            .format(inventory_path=inventory_path,
                    playbook_path=self.playbook_path,
                    extra_vars=json.dumps(definitive_group_vars),
                    before=' '.join(before)),
            stdout=asyncio.subprocess.PIPE,
            loop=self.loop
        )

        while True:
            try:
                while not process.stdout.at_eof():
                    logger.debug(await process.stdout.readline())
                    # TODO: may as well check for the output.

                await asyncio.wait_for(process.wait(), 0.5)
                break
            except asyncio.TimeoutError:
                continue
