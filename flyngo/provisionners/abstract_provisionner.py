from typing import Dict, Optional, Any, List
from flyngo.servers import Server, Instance

ProvisionnerOptions = Optional[Dict[str, Any]]


class ProvisionnerConfigurationError(RuntimeError):
    pass


class AbstractProvisionner:
    def __init__(self, provisionner_opts: ProvisionnerOptions = None):
        if provisionner_opts is None:
            self.provisionner_opts = {}
        else:
            self.provisionner_opts = provisionner_opts

    async def provision(self,
                        server: Server,
                        extra_parameters: List[Dict[str, Any]] = None,
                        single: bool = False,
                        fresh: bool = False) -> List[Instance]:
        """
        Provision a server using a set of commands.

        :param server: Server to provision.
        :type server: Server
        :param extra_parameters: a list of custom parameters for the provisionner (per server).
        :type extra_parameters: List[Dict[str, str]]
        :param single: do we deploy a single instance?
        :type single: bool
        :param fresh: is the machine just deployed right now?
        :type fresh: bool
        :return: None
        :rtype: NoneType
        """
        raise NotImplementedError
