from enum import Enum
from typing import Tuple, Optional


class GameModes(Enum):
    ArmsRace = 1
    ClassicCasual = 2
    ClassicCompetitive = 3
    Demolition = 4
    Deathmatch = 5


def translate_game_mode_enum(item: GameModes) -> Optional[Tuple[int, int]]:
    """
    Translate a Game Mode item (from enum) into a tuple game_type, game_mode for config purpose.

    :param item: A Game Mode item
    :type item: GameModes
    :return: game_type, game_mode, None if the item is not an enum.
    :rtype: Tuple[int, int] or NoneType
    """
    if item == GameModes.ArmsRace:
        return 1, 0
    elif item == GameModes.ClassicCasual:
        return 0, 0
    elif item == GameModes.ClassicCompetitive:
        return 0, 1
    elif item == GameModes.Demolition:
        return 1, 1
    elif item == GameModes.Deathmatch:
        return 1, 2
    else:
        return None
