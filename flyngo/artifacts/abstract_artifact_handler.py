from typing import Dict, Optional, List
from flyngo.storage.abstract_storage import AbstractStorage
import os.path
import logging

ArtifactOptions = Optional[Dict[str, str]]

FIND_DEM_FILES = 'find -L {path} -name "*.dem" 2>/dev/null'
LOCATE_DEM_FILES = 'locate *.dem'

logger = logging.getLogger(__name__)


class AbstractArtifactHandler:
    def __init__(self, storage: AbstractStorage, artifact_opts: ArtifactOptions = None):
        if artifact_opts is None:
            self.artifact_opts = {}
        else:
            self.artifact_opts = artifact_opts

        self.storage = storage

    async def search_for_dem_files(self,
                                   root,
                                   folders: Optional[List[str]] = None,
                                   locate: bool=False) -> List[str]:
        """
        Search for DEM files through the filesystem tree.
        :param root: a connection to the target server.
        :type root: SSHConnection
        :param folders: folders to search in (default to `dem_files_folders` configuration or root tree)
        :type folders: Optional[List[str]]
        :param locate: if you have ensured that your filesystem will cache your new DEM files, then are locate-ables,
        use locate for faster searches (by default: false)
        :type locate: bool
        :return: a list of dem files paths
        :rtype: List[str]
        """
        if not locate and not folders:
            folders = self.artifact_opts.get('dem_files_folders', ['/'])

        dem_files = []

        if locate:
            result = await root.execute(LOCATE_DEM_FILES)
            for path in result.stdout.split('\n'):
                dem_files.append(path)
        else:
            for folder in folders:
                logger.info('Looking in {} for DEM files'.format(folder))
                result = await root.execute(FIND_DEM_FILES.format(path=folder))
                for path in result.stdout.split('\n'):
                    dem_files.append(path)

        return dem_files

    async def download_files(self,
                             root,
                             paths: List[str],
                             target_folder: Optional[str]=None) -> List[str]:
        """
        Download files using SFTP subsystem on the server machine.

        :param root: Target SSH connection
        :type root: SSHConnection
        :param paths: List of files to download
        :type paths: List[str]
        :param target_folder: Target folder
        :type target_folder: Optional[str]
        :return: list of new paths on the local host
        :rtype: List[str]
        """
        if target_folder is None:
            target_folder = self.artifact_opts.get('target_folder', '/tmp/')

        sftp = await root.engage_sftp_subsystem()
        results = []

        for path in paths:
            try:
                await sftp.get(path, localpath=target_folder)
                filename = os.path.basename(path)
                results.append(os.path.join(target_folder, filename))
                logger.info('Downloaded {} in /tmp'.format(filename))
            except:
                logger.error('Failed to download DEM: {}'.format(path))

        return results

    async def upload(self, server) -> None:
        """
        Upload artifacts to the underlying storage.

        :param server: a server instance hosting a game
        :type server: Server
        :return: None
        :rtype: NoneType
        """
        raise NotImplementedError
