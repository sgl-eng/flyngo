import asyncio
from collections import deque, defaultdict
from typing import Dict, Any
import re

loop = asyncio.get_event_loop()

class UnknownEventError(RuntimeError):
    pass


def populate_event_types() -> Dict[str, Any]:
    from .srcds_events import STANDARD_EVENTS, CSGO_EVENTS
    event_types = {}
    for cls in STANDARD_EVENTS + CSGO_EVENTS:
        regex = re.compile(cls.regex, re.U)
        event_types[cls.__name__.lower()] = regex, cls

    return event_types


MAGIC_HEADER_1 = b'\xff\xff\xff\xffRL'
MAGIC_HEADER_2 = b'\xff\xff\xffRL'


class SourceLogProtocol(asyncio.DatagramProtocol):
    EVENTS_TYPE = populate_event_types()

    def __init__(self, skip_unknown: bool=True):
        self._transport = None
        self.events = deque()
        self.skip_unknown = skip_unknown

        self.current_buffer = bytes()
        self.events_callback = defaultdict(list)

    def connection_made(self, transport):
        self._transport = transport

    def on_event(self, name, callback):
        assert name.lower() in self.EVENTS_TYPE.keys()
        self.events_callback[name.lower()].append(callback)

    def run_callbacks(self, event):
        for cb in self.events_callback[event.__name__.lower()]:
            cb(event)

    def line_received(self, line):
        line = line.strip()
        for rgx, cls in self.EVENTS_TYPE.values():
            match = rgx.match(line)
            if match:
                event = cls.from_re_match(match)
                self.run_callbacks(event)
                self.events.append(event)
                return

        if not self.skip_unknown:
            raise UnknownEventError('Could not parse event: {}'.format(line))

    def datagram_received(self, data, addr):
        # concatenate new data.
        # strip useless data.
        if data.startswith(MAGIC_HEADER_1):
            data = data[len(MAGIC_HEADER_1):]
        elif data.startswith(MAGIC_HEADER_2):
            data = data[len(MAGIC_HEADER_2):]

        self.current_buffer += data
        self.current_buffer = self.current_buffer.decode('ascii')

        # split into buffers by \n delimiter.
        buffers = self.current_buffer.split('\n\x00')
        # the new current buffer is the last buffer.
        self.current_buffer = buffers[-1].encode('ascii')
        # the rest is lines.
        for line in buffers[:-1]:
            self.line_received(line)

    def error_received(self, exc):
        pass


def connection_requested():
    return SourceLogProtocol()


async def run_server():
    transport, protocol = await loop.create_datagram_endpoint(
        connection_requested,
        local_addr=('0.0.0.0', 12362)
    )

    return transport, protocol

if __name__ == '__main__':
    loop.run_until_complete(run_server())
    loop.run_forever()