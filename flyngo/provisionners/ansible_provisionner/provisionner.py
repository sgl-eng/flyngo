from typing import Dict, Optional, Any, Iterable, List, Tuple
from flyngo.servers import Server, Instance
import uuid
import os.path
from jinja2 import Template
import logging

from flyngo.provisionners.abstract_provisionner import AbstractProvisionner, ProvisionnerConfigurationError
from flyngo.utils import generate_random_password
from .ansible_wrapper import AnsibleWrapper
from .user_connection import UserConnection

ProvisionnerOptions = Optional[Dict[str, Any]]
MAX_INSTANCES = 100

logger = logging.getLogger('flyngo.ansible_provisionner')


class AnsibleProvisionner(AbstractProvisionner):
    def __init__(self, provisionner_opts: ProvisionnerOptions = None):
        super().__init__(provisionner_opts)
        if 'playbook_path' not in self.provisionner_opts:
            raise ProvisionnerConfigurationError('Ansible require a playbook to provision a set of machines!')
        if 'config_template_path' not in self.provisionner_opts:
            raise ProvisionnerConfigurationError('Ansible require a config template path!')
        if 'gslts' not in self.provisionner_opts:
            raise ProvisionnerConfigurationError('Ansible require GSLTs!')

        self.playbook_path = self.provisionner_opts['playbook_path']
        self.private_key = self.provisionner_opts['private_key_path']
        with open(self.provisionner_opts['config_template_path'], 'r') as f:
            template_data = f.read()
        self.config_template = Template(template_data)
        self.wrapper = AnsibleWrapper(self.playbook_path, self.private_key)
        self.gameserver_port_generator = iter(range(27015, 27015 + MAX_INSTANCES))
        self.sourcetv_port_generator = iter(range(28015, 28015 + MAX_INSTANCES))
        self.client_port_generator = iter(range(29015, 29015 + MAX_INSTANCES))
        self.gslts = self.provisionner_opts['gslts']
        self.available_gslts = self.gslts.copy()
        logger.info('Ansible provisionner ready, {} GSLTS available'
                    .format(len(self.gslts)))

    @staticmethod
    def _check_skeleton(data: Dict[str, Any]) -> None:
        if 'hostname' not in data:
            raise RuntimeError('extra_parameters must contain hostname in dicts')

    def write_config_files(self,
                           server: Server,
                           extra_parameters: Iterable[Dict[str, Any]]) -> Tuple[Dict[str, str], Dict[str, Instance]]:
        uuid_path = '/tmp/config_{}/'.format(uuid.uuid4())
        configs = {}
        paths = {}
        instances = {}

        os.makedirs(uuid_path, exist_ok=True)

        for extra_parameter in extra_parameters:
            hostname = extra_parameter['hostname']
            ctx = {
                'hostname': hostname,
                'rcon_password': extra_parameter.get(
                    'rcon_password',
                    generate_random_password()
                ),
                'server_password': extra_parameter.get(
                    'server_password',
                    generate_random_password()
                ),
                'gotv_relay_password': extra_parameter.get(
                    'gotv_relay_password',
                    generate_random_password()
                )
            }
            instance = Instance(
                server,
                server.connection_parameters['hostname'],
                ctx['rcon_password'],
                ctx['server_password'],
                hostname=hostname
            )

            rendered_config = self.config_template.render(ctx)
            configs[hostname] = rendered_config
            instances[hostname] = instance

        for index, (hostname, config) in enumerate(configs.items()):
            filename = 'sgl17_server_{}.conf'.format(index)
            path = os.path.join(uuid_path, filename)
            paths[hostname] = path
            with open(path, 'w') as f:
                f.write(config)

        return paths, instances

    def generate_ports(self):
        return (
            next(self.gameserver_port_generator),
            next(self.sourcetv_port_generator),
            next(self.client_port_generator)
        )

    def generate_group_variables(self,
                                 config_files: Dict[str, str],
                                 instances: Dict[str, Instance]) -> Dict[str, Any]:
        if len(self.available_gslts) < len(config_files):
            raise RuntimeError('Not enough GSLTs!')

        servers_list = []
        for index, (hostname, server_config) in enumerate(config_files.items()):
            gameserver_port, sourcetv_port, client_port = self.generate_ports()
            server_dict = {
                'user': 'csgo{}'.format(index),
                'gslt': self.available_gslts.pop(0),
                'service_name': 'csgo-server-{}'.format(index),
                'config_path': server_config,
                'port': gameserver_port,
                'sourcetv_port': sourcetv_port,
                'client_port': client_port
            }
            instances[hostname].port = gameserver_port
            instances[hostname].sourcetv_port = sourcetv_port
            instances[hostname].extra['username'] = server_dict['user']
            instances[hostname].extra['gslt'] = server_dict['gslt']

            servers_list.append(server_dict)

        return {'servers': servers_list}

    async def provision(self,
                        server: Server,
                        extra_parameters: Optional[Iterable[Dict[str, Any]]] = None,
                        single: bool = False,
                        fresh: bool = False) -> List[Instance]:
        """
        Provision a server using Ansible and a playbook.

        :param server: Server to provision.
        :type server: Server
        :param extra_parameters: a list of custom parameters [{hostname, gslt: optionally}]
        :type extra_parameters: List[Dict[str, str]]
        :param single: do we deploy a single instance?
        :type single: bool
        :param fresh: is the machine just deployed right now?
        :type fresh: bool
        :return: None
        :rtype: NoneType
        """
        logger.info('Provisionning the server: {}'.format(server.id))
        if not extra_parameters:
            raise RuntimeError('Ansible needs extra_parameters to do his job!')

        for parameters in extra_parameters:
            self._check_skeleton(parameters)

        config_files, instances = self.write_config_files(server, extra_parameters)
        variables = self.generate_group_variables(config_files, instances)

        for instance in instances.values():
            instance.setup_connection(UserConnection)

        logger.info('Now, running Ansible on {}!'.format(server.connection_parameters['hostname']))
        await self.wrapper.run(
            server.connection_parameters['hostname'],
            variables
        )

        logger.info('Ansible finished, injecting back the control system.')
        for instance in instances.values():
            await instance.boot_connection()

        logger.info('Total instances provisionned: {}.'.format(len(instances)))
        return list(instances.values())
