from typing import NewType, Dict, Any
from flyngo.drivers.abstract_driver import AbstractDriver
from flyngo.artifacts.abstract_artifact_handler import AbstractArtifactHandler
from flyngo.provisionners.abstract_provisionner import AbstractProvisionner
from flyngo.storage.abstract_storage import AbstractStorage
import importlib
import inspect

Importable = NewType('Importable', str)

importable_table = {
    'driver': AbstractDriver,
    'artifact_handler': AbstractArtifactHandler,
    'provisionner': AbstractProvisionner,
    'storage': AbstractStorage
}

skeleton = {
    'driver': Importable,
    'driver_opts': Dict[str, Any],
    'provisionner': Importable,
    'provisionner_opts': Dict[str, Any],
    'artifact_handler': Importable,
    'artifact_opts': Dict[str, Any],
    'storage': Importable,
    'storage_opts': Dict[str, Any],
    'game_opts': Dict[str, Any],
    'extra_settings': Dict[str, Any]
}


class ConfigurationError(ValueError):
    pass


def load_class(value):
    module_path = '.'.join(value.split('.')[:-1])
    class_name = value.split('.')[-1]
    return getattr(importlib.import_module(module_path), class_name)


def inherits_from(child, parent_name):
    if inspect.isclass(child):
        if parent_name in [c.__name__ for c in inspect.getmro(child)[1:]]:
            return True

    return False


def verify_configuration(config: Dict[str, Any]) -> None:
    for k, v in config.items():
        if k in skeleton and skeleton[k] is Importable:
            mod = load_class(v)
            if mod is None:
                raise ConfigurationError('{} does not exists!'.format(v))
            if not inherits_from(mod, importable_table[k].__name__):
                raise ConfigurationError('Invalid {} which does not inherit from {} !'
                                         .format(v, importable_table[k].__name__))
        else:
            pass
    # TODO: enforce typing?

    config['__verified'] = True


def resolve_configuration(config_dict: Dict[str, Any]) -> Dict[str, Any]:
    new_config = {}

    for k, sk_type in skeleton.items():
        v = config_dict.get(k)
        if sk_type is Importable:
            new_config[k] = load_class(v)
        else:
            new_config[k] = v

    return new_config
