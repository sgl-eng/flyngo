from typing import Optional, Dict, List, Union, io

StorageOptions = Optional[Dict[str, str]]


class AbstractStorage:
    def __init__(self, storage_opts: StorageOptions = None):
        if storage_opts is None:
            self.storage_opts = {}
        else:
            self.storage_opts = storage_opts

    def save(self, file: Union[io, str]) -> None:
        """
         Save a file to the storage.
        :param file: The file
        :type file: io (file-like) or str (path)
        :return: None
        :rtype: NoneType
        """
        raise NotImplementedError

    def batch_save(self, files: Union[List[io], List[str]]) -> None:
        """
        Save multiple files to the storage.
        :param files: A list of files
        :type files: List[io] or List[str]
        :return: None
        :rtype: NoneType
        """
        raise NotImplementedError
