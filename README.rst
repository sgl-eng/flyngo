========
Fly'n'Go
========

.. image:: https://badge.fury.io/py/flyngo.png
    :target: http://badge.fury.io/py/flyngo

.. image:: https://pypip.in/d/flyngo/badge.png
    :target: https://pypi.python.org/pypi/flyngo


Launch CS:GO servers for your tournaments on the fly using a baremetal cloud provider.


Features
--------

* Request a CS:GO server to be deployed asynchronously.

Examples
--------

.. code-block:: python

    import flyngo
    cs_config = None
    with open('./cs_config.conf', 'r') as f:
        cs_config = f.read()

    settings = {
        # Use Scaleway to launch baremetal servers.
        'driver': 'flyngo.drivers.ScalewayDriver',
        # C2M is sufficient for CS:GO servers.
        'driver_opts': {
            'auth_token': '1203812301824018248012408124',
            'machine_size': 'C2M',
            # Image to provision the machine from.
            # `csgo-server` has to be an already-created image.
            # You can specify any distribution and use a custom provisioning process.
            'image_name': 'CS:GO Server',
            # Or, you can specify the ID directly, it saves a request.
            'image_id': '1230812408124'
        },
        'provisionner': 'flyngo.provisionner.LGSMProvisionner',
        'provisionner_opts': {
            'already_installed': True,
            'lgsm_account': 'csgoserver',
            'start_map': 'de_dust2',
            'mapgroup': 'random_classic',
            'workshop_collection_id': '1248010284',
            'workshop_start_map': '1284018024'
        }
        # The Artifact handler to download DEM files, etc…
        'artifact_handler': 'flyngo.artifacts.CSGOArtifacts',
        'artifact_opts': {
        },
        # Where do we store the DEM files?
        'storage_handler': 'flyngo.storage.S3Storage',
        'storage_opts': {
            'auth_id': '1240812408'
         }
        'game_opts': {
            # Because.
            'tickrate': 128,
            'custom_cfg_content': cs_config
        }
    }

    flyngo.initialize_settings(settings)

    async def start_match(map_pool, players, admins):
        hostname = '[SGL] {} vs {}'.format(players.team_1, players.team_2)
        password = None # why not?
        server = await flyngo.start_server(map_pool, hostname, password)
        await server.ensure_ready()
        discord.send_message(admins, 'The match is ready on {}'.format(server.steam_url))
        discord.send_message(players, 'The match starts now, join it on {} !'.format(server.steam_url))
        captains = filter_captains(players)
        tournaments_manager.add_match(CSGO_GAME, players, server)
        server.on_finished(end_match, extra={'captains': captains})

    def end_match(server, extra):
        captains = extra.get('captains')
        if captains is None:
            raise RuntimeError('Expected captains when the match is finished!')

        left, right = discord.retrieve_score(captains, 'How was the match? What was the score of your team and the enemy\'s team?')

        if left != right:
            discord.report_problem('Score mismatch on the match: {}'.format(server.match_info))
            score = discord.admin_retrieve_score(server.match_id)
            tournament.register_score(score, server.match_id)
        else:
            toornament.register_score(left, server.match_id)
