import os
from base64 import b64encode


def generate_random_password():
    return (
        b64encode(os.urandom(18)).decode('utf8')
            .replace('/', 'z')
            .replace('+', 'u')
    )
