from .abstract_driver import AbstractDriver, DriverOptions
from scaleway.apis import ComputeAPI, AccountAPI
from flyngo.servers import Server
from haikunator import Haikunator
import asyncio
import time


haikunator = Haikunator()


class MissingImage(RuntimeError):
    pass


class ScalewayDriver(AbstractDriver):
    DEFAULT_MACHINE_SIZE = 'C2S'

    def __init__(self,
                 driver_opts: DriverOptions,
                 loop: asyncio.AbstractEventLoop = None):
        super().__init__(driver_opts, loop)

        self.driver_opts.setdefault('booted_tags', ['flyngo'])
        self.driver_opts.setdefault('unused_tags', ['flyngo-free'])

        self.servers_pool = {}
        self.unused_servers = []
        self.api = ComputeAPI(auth_token=driver_opts['auth_token'])

    @staticmethod
    def supports_inspection() -> bool:
        return True

    def _map_to_server(self, server_object):
        """
        Map a Scaleway response object of a server to a proper Server instance.

        :param server_object: the Scaleway response dict
        :type server_object: Dict[str, Any]
        :return: the tied server instance
        :rtype: Server
        """
        server_id = server_object['id']
        verbose_name = server_object['name'][len('flyngo-'):]
        connection_parameters = {
            'hostname': server_object['public_ip']['address'],
            'username': 'root',
            'port': 22
        }
        return Server(server_id,
                      self,
                      connection_parameters,
                      verbose_name=verbose_name)

    async def inspect(self):
        resp = await self.loop.run_in_executor(
            None,
            self.api.query().servers.get
        )

        servers = resp['servers']

        unused_tags = self.driver_opts['unused_tags']
        booted_tags = self.driver_opts['booted_tags']

        flyngo_servers = {server['id']: server for server in servers if set(booted_tags) <= set(server['tags'])}
        self.servers_pool = flyngo_servers

        used_servers = [self._map_to_server(server)
                        for server in flyngo_servers.values()
                        if set(server['tags']).isdisjoint(set(unused_tags))]

        self.unused_servers = [self._map_to_server(server)
                               for server in flyngo_servers.values()
                               if set(unused_tags) <= set(server['tags'])]

        return used_servers, self.unused_servers

    @property
    def image_id(self):
        return self.driver_opts.get('image_id')

    @property
    def organization_id(self):
        return self.driver_opts.get('organization_id')

    @property
    def machine_size(self):
        return self.driver_opts.get('machine_size',
                                    self.DEFAULT_MACHINE_SIZE)

    async def look_for_image(self, image_name):
        resp = await self.loop.run_in_executor(
            None,
            self.api.query().images.get
        )
        images = resp['images']

        matching_images = [
            image for image in images
            if image['name'].lower() == image_name.lower()
        ]

        if matching_images:
            image = matching_images[0]
            self.driver_opts['image_id'] = image['id']
            return image['id']
        else:
            raise MissingImage('Image ID not found!')

    async def get_org_id(self):
        api = AccountAPI(auth_token=self.driver_opts['auth_token'])
        resp = await self.loop.run_in_executor(
            None,
            api.query().organizations.get
        )

        organizations = resp['organizations']
        self.driver_opts['organization_id'] = organizations[0]['id']
        return self.driver_opts['organization_id']

    def wait_for_server_to_boot(self, machine_data):
        machine_id = machine_data['id']

        resp = self.api.query().servers(machine_id).get()
        resp = resp['server']
        while (
                    (not resp['state'] == 'running')
                or
                    (not resp['public_ip'])):
            resp = self.api.query().servers(machine_id).get()
            resp = resp['server']
            time.sleep(1)

        return resp

    def has_unused_servers(self) -> bool:
        return len(self.unused_servers) > 0

    def pick_a_free_server(self) -> Server:
        unused_server = self.unused_servers.pop(0)
        unused_server.catch()
        self.api.query().servers(unused_server.id).put({
            'tags': self.driver_opts['booted_tags']
        })
        return unused_server

    def recycle(self, server: Server) -> None:
        self.unused_servers.append(server)
        self.api.query().servers(server.id).put({
            'tags': self.driver_opts['booted_tags'] + self.driver_opts['unused_tags']
        })

    def _stop_and_destroy(self, server_id: str) -> None:
        self.api.query().servers(server_id).action.post({
            'action': 'poweroff'
        })
        self.api.query().servers(server_id).delete()

    async def destroy(self, server: Server) -> None:
        await self.loop.run_in_executor(
            None,
            self._stop_and_destroy,
            server.id
        )

    async def fire_up_a_machine(self) -> Server:
        name = haikunator.haikunate(
            delimiter='-',
            token_length=0
        )
        start = time.time()
        image_id = self.image_id
        org_id = self.organization_id
        if not image_id:
            image_id = await self.look_for_image(self.driver_opts.get('image_name'))
        if not org_id:
            org_id = await self.get_org_id()

        hostname = 'flyngo-{}'.format(name)
        machine_settings = {
            'name': hostname,
            'organization': org_id,
            'volumes': {},
            'image': image_id,
            'commercial_type': self.machine_size,
            'tags': self.driver_opts['booted_tags']
        }
        response = await self.loop.run_in_executor(
            None,
            self.api.query()
            .servers().post,
            machine_settings
        )
        response = response['server']
        new_id = response['id']

        self.servers_pool[new_id] = response

        # Create a new IP.
        ip_resp = await self.loop.run_in_executor(
            None,
            self.api.query()
            .ips().post,
            {
                'organization': org_id
            }
        )

        ip_resp = ip_resp['ip']

        # Many useless fields… Thanks Scaleway's stupidity.
        ip_settings = {
            'server': new_id,
            'organization': org_id,
            'id': ip_resp['id'],
            'address': ip_resp['address'],
            'reverse': None
        }

        # Attach a public IP.
        await self.loop.run_in_executor(
            None,
            self.api.query()
            .ips(ip_resp['id']).put,
            ip_settings
        )

        # Power on the machine.
        await self.loop.run_in_executor(
            None,
            self.api.query()
            .servers(new_id).action
            .post,
            {'action': 'poweron'}
        )

        # Wait for the machine to be ready.
        await self.loop.run_in_executor(
            None,
            self.wait_for_server_to_boot,
            response
        )

        connection_parameters = {
            'hostname': ip_resp['address'],
            'port': 22,
            'username': 'root'
        }

        elapsed = time.time() - start

        return Server(new_id, self, connection_parameters,
                      verbose_name=name,
                      creation_speed=elapsed)
