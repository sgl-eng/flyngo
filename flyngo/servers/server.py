from typing import Dict, Awaitable, Any, List
import asyncssh
import os
import json
from asyncio import Future, Handle
from flyngo.servers.instance import Instance
from flyngo.servers.ssh_connection import SSHConnection


class Server:
    def __init__(self,
                 unique_id: str,
                 driver,
                 connection_parameters: Dict[str, Any],
                 capacity: int = 1,
                 creation_speed: float = None,
                 verbose_name: str = ''):
        """
        Create a new server instance.
        :param unique_id: Driver-specific unique identifier (good candidate for Driver API ID)
        :type unique_id: str
        :param driver: The driver which manages this server
        :type driver: subclass of flyngo.drivers.abstract_driver.AbstractDriver
        :param connection_parameters: all parameters needed to connect to the server through SSH
        :type connection_parameters: Dict[str, Any]
        :param capacity: How many instances of SRCDS can this server supports?
        :type capacity: integer (default 1)
        :param creation_speed: time required for the server to be ready
        :type creation_speed: float
        :param verbose_name: a verbose name for debugging / provisionner
        :type verbose_name: str
        """
        self._driver = driver
        self._client = None
        self.id = unique_id
        self.capacity = capacity
        self.connection_parameters = connection_parameters
        self.instances = []  # type: List[Instance]
        self.on_finished_callbacks = []
        self.creation_speed = creation_speed
        self.verbose_name = verbose_name
        # An asyncio.Handle to cancel the callback.
        self.garbage_collection_handle = None
        self.artifact_handler = None

        self.on_finished = Future()

    def __str__(self):
        return (
            '<Server ID: {}, '
            'capacity: {}, '
            'parameters: {}, '
            'instances: {}, '
            'creation_speed: {}, '
            'verbose_name: {}>'
        ).format(self.id, self.capacity, self.connection_parameters, ', '.join(list(map(str, self.instances))),
                 self.creation_speed if self.creation_speed else 'N/A',
                 self.verbose_name if self.verbose_name else 'N/A')

    def to_dict(self):
        return {
            '_driver': self._driver.__class__.__name__,
            'id': self.id,
            'capacity': self.capacity,
            'connection_parameters': self.connection_parameters,
            'instances': [instance.to_dict() for instance in self.instances],
            'creation_speed': self.creation_speed,
            'verbose_name': self.verbose_name
        }

    @classmethod
    def from_dict(cls, dict_repr, driver, artifact_handler):
        assert dict_repr['_driver'] == driver.__class__.__name__, 'Driver class name does not match!'

        server = Server(
            dict_repr['id'],
            driver,
            dict_repr['connection_parameters'],
            dict_repr['capacity'],
            dict_repr['creation_speed'],
            dict_repr['verbose_name']
        )

        server.instances = [Instance.from_dict(inst_repr, server) for inst_repr in dict_repr['instances']]
        server.artifact_handler = artifact_handler

        return server

    def to_json(self):
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, serialized, driver, artifact_handler):
        return cls.from_dict(json.loads(serialized), driver, artifact_handler)

    async def connect_as_root(self) -> SSHConnection:
        """
            Open an SSH connection (as a root account) to the server.
            Cache the connection for future recycling.

        :return: the SSHConnection wrapper
        :rtype: SSHConnection
        """
        if not self._client:
            c_params = self.connection_parameters.copy()
            if 'hostname' in c_params:
                del c_params['hostname']
            # Handle retry.
            private_key = self._driver.driver_opts['private_key'] or None
            agent_path = os.environ.get('SSH_AUTH_SOCK', None)

            extra_params = {
                'agent_path': agent_path,
                'known_hosts': None,
                'client_keys': [private_key] if private_key else None
            }
            client = await asyncssh.connect(self.connection_parameters['hostname'],
                                            **c_params,
                                            **extra_params)

            self._client = SSHConnection(client, extra_params)
            # Ad-hoc dependency installation!
            await self._client.execute('apt-get install -y patch || yum -y install patch')
            await self._client.read_machine_data()

        return self._client

    async def ensure_ready(self) -> bool:
        """
        Ensure that the server is reachable and ready to accept any commands.

        Sometimes, the server can hang up, or being occupied by another task.

        :return: True if the server is ready, False otherwise.
        :rtype: bool
        """
        c = await self.connect_as_root()
        result = await c.execute('whoami')
        content = result.stdout
        return content == self.connection_parameters['username']  # Sanity test.

    async def upload_artifacts(self) -> Awaitable:
        """
        Upload artifacts using the Artifact handler.

        :return: An artifact upload result.
        :rtype: Awaitable
        """
        res = await self.artifact_handler.upload(self)
        return res

    async def release(self):
        """
        Release this machine.

        :return: None
        :rtype: NoneType
        """
        await self._driver.destroy(self)

    def free(self, handle: Handle):
        """
        Free machine usage, consider it for recycling.
        :return: None
        :rtype: NoneType
        """
        self.garbage_collection_handle = handle
        self.capacity += 1
        self._driver.recycle(self)

    def catch(self):
        """
        Catch machine, cancel the garbage collection.

        :return: None
        :rtype: NoneType
        """
        self.capacity -= 1
        if self.garbage_collection_handle:
            self.garbage_collection_handle.cancel()

    async def update_capacity(self, new_value: int):
        """
        Update capacity, can reflect on the driver if the driver supports it.

        :param new_value: new capacity
        :type new_value: positive integer
        :return: self
        :rtype: Server
        """
        self.capacity = new_value
        if self._driver.supports_capacity():
            await self._driver.update_capacity(self, new_value)

        return self

    async def add_instances(self, instances: List[Instance]):
        """
        Bind instances to the current server machine.
        Assert that the number of instances to bind is less or equal to the current capacity.

        :param instances: the list of instances to bind
        :type instances: List[Instance]
        :return: None
        :rtype: NoneType
        """
        assert self.capacity >= len(instances)

        await self.update_capacity(self.capacity - len(instances))
        for instance in instances:
            instance.server = self
        self.instances.extend(instances)
