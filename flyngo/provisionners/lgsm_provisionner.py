from .abstract_provisionner import AbstractProvisionner, ProvisionnerOptions
from flyngo.servers import Server, SSHConnection, Instance
from typing import Dict, List, Any
from flyngo.utils import generate_random_password
from flyngo.utils.constants import GameModes, translate_game_mode_enum
from flyngo.utils.lgsm_account import LGSMAccount

BASE_CSGO_CONFIGURATION = """
// Hostname for server.
hostname "{hostname}"

// RCON - remote console password.
rcon_password "{rcon_password}"

// Server password - for private servers.
sv_password "{server_password}"

// Server Logging
log on
sv_logbans 1
sv_logecho 1
sv_logfile 1
sv_log_onefile 0

// Server Hibernation
sv_hibernate_when_empty 1
sv_hibernate_ms 5

// Server Query
// More info at: https://www.gametracker.com/games/csgo/forum.php?thread=91691
host_name_store 1
host_info_show 1
host_players_show 2

// Custom configuration
{custom_cfg_content}

// GOTV
tv_port "27020"
tv_enable "1"
tv_title "{tv_title}"
tv_snapshotrate "128"
tv_delay "0"

exec banned_user.cfg
exec banned_ip.cfg
writeid
writeip
"""

DEFAULT_GAME_MODE = GameModes.ClassicCasual


# Linux Game Servers Managers Provisionner.


class LGSMProvisionner(AbstractProvisionner):
    """
        Use https://gameservermanagers.com/ to provision the machine.
    """

    def __init__(self, provisionner_opts: ProvisionnerOptions = None):
        super().__init__(provisionner_opts)
        self.already_installed = self.provisionner_opts.get('already_installed', False)
        self.lgsm_account = self.provisionner_opts.get('lgsm_account', 'csgoserver')

        self.use_workshop_for_maps = self.provisionner_opts.get('workshop_collection_id', None) is not None
        self.use_mapgroup = self.provisionner_opts.get('mapgroup', None) is not None

        if self.use_workshop_for_maps and self.use_mapgroup:
            raise RuntimeError('Invalid provisionner configuration: cannot use mapgroup and workshop at the same time!')

        if self.use_workshop_for_maps and self.provisionner_opts('workshop_auth_key') is None:
            raise RuntimeError('Invalid provisionner configuration: no workshop auth key provided for maps')

    async def install_lgsm(self, root: SSHConnection) -> LGSMAccount:
        # TODO: support Fedora and CentOS.
        # For Ubuntu / Debian.
        assert root.machine_data['distribution'].lower() in ('ubuntu', 'debian')
        await root.execute('dpkg --add-architecture i386')
        await root.execute('apt-get update')
        packages = [
            'mailutils',
            'postfix',
            'curl',
            'wget',
            'file',
            'bzip2',
            'gzip',
            'unzip',
            'bsdmainutils',
            'python',
            'util-linux',
            'tmux',
            'lib32gcc1',
            'libstdc++6',
            'libstdc++6:i386'
        ]
        await root.execute('apt-get install {}'.format(' '.join(packages)))
        await root.add_user(self.lgsm_account, generate_random_password())

        lgsm = LGSMAccount(await root.su(self.lgsm_account))
        await lgsm.install_lgsm()

        return lgsm

    async def provision(self,
                        server: Server,
                        extra_parameters: List[Dict[str, Any]] = None,
                        single: bool = False,
                        fresh: bool = False) -> List[Instance]:
        assert single

        extra_parameters = extra_parameters[0]
        root = await server.connect_as_root()

        recently_installed = not self.already_installed
        if not self.already_installed:
            csgo = await self.install_lgsm(root)
        else:
            csgo = LGSMAccount(await root.su(self.lgsm_account))

        # Set up the interface.
        ip = await root.get_primary_ip('eth0')
        await csgo.replace_basic_config('ip', ip)
        await csgo.replace_basic_config('port', extra_parameters.get('port', 27015))
        await csgo.replace_basic_config('tickrate', extra_parameters.get('tickrate', 64))

        if self.use_mapgroup:
            mapgroup = extra_parameters.get('map_group', self.provisionner_opts.get('map_group'))
            await csgo.replace_basic_config('mapgroup', mapgroup)

        default_map = extra_parameters.get('default_map', self.provisionner_opts.get('default_map'))
        if default_map:
            await csgo.replace_basic_config('defaultmap', default_map)

        game_name = extra_parameters.get('game_mode', self.provisionner_opts.get('game_mode'))
        if not game_name:
            game_name = DEFAULT_GAME_MODE
        else:
            game_name = GameModes[game_name]

        game_type, game_mode = translate_game_mode_enum(game_name)
        await csgo.replace_basic_config('gametype', game_type)
        await csgo.replace_basic_config('gamemode', game_mode)

        # Prefer to put the GSLT immediately before the provisionning (image installation).
        if 'gslt' in extra_parameters:
            await csgo.replace_basic_config('gslt', extra_parameters['gslt'])

        if self.use_workshop_for_maps:
            authkey = self.provisionner_opts.get('workshop_auth_key')
            collection_id = self.provisionner_opts.get('workshop_collection_id')
            start_map = self.provisionner_opts.get('workshop_start_map')

            await csgo.replace_basic_config('authkey', authkey)
            await csgo.replace_basic_config('ws_collection_id', collection_id)
            await csgo.replace_basic_config('ws_start_map', start_map)
        else:
            await csgo.comment_option('authkey')
            await csgo.comment_option('ws_collection_id')
            await csgo.comment_option('ws_start_map')

        server_password = generate_random_password().decode('utf8')
        rcon_password = generate_random_password().decode('utf8')

        extra_config = {
            'hostname': extra_parameters.get('hostname', 'flyngo {}'.format(server.verbose_name)),
            'rcon_password': rcon_password,
            'server_password': server_password,
            'custom_cfg_content': extra_parameters.get('custom_cfg_content', '')
        }

        extra_config['tv_title'] = extra_parameters.get('tv_title', extra_config['hostname'])

        await csgo.insert_extra_config(BASE_CSGO_CONFIGURATION
                                       .format(**extra_config))

        await csgo.stop()
        if not recently_installed:
            await csgo.update_lgsm()

        await csgo.update_server_files()
        await csgo.start()

        return [
            Instance(
                server,
                ip,
                rcon_password,
                server_password,
                extra_parameters.get('port', 27015)
            )
        ]
