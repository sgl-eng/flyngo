from .abstract_driver import AbstractDriver, DriverOptions
import packet
from flyngo.servers import Server
from haikunator import Haikunator
from functools import partial
import asyncio
import time
import logging

logger = logging.getLogger('flyngo.packet_driver')

# TODO: implement capacity retrieval.
# idea: use tags to update the capacity on realtime.


haikunator = Haikunator()


class PacketDriver(AbstractDriver):
    # An Eva Gundam.
    DEFAULT_MACHINE_SIZE = 'baremetal_1'  # Type 1.

    # An Eva Gundam is an Eva Gundam after all.
    DEFAULT_CAPACITY_PER_MACHINE = 10

    # Facility
    DEFAULT_FACILITY = 'ams1'

    # Operating System
    DEFAULT_OPERATING_SYSTEM = 'centos_7'

    # FIXME: should support +inf machines.
    MAX_MACHINES = 100

    STARTING_CAPACITY = {
        'baremetal_1': 20
    }

    def __init__(self,
                 driver_opts: DriverOptions,
                 loop: asyncio.AbstractEventLoop = None):
        super().__init__(driver_opts, loop)

        self.driver_opts.setdefault('booted_tags', [])
        self.driver_opts.setdefault('unused_tags', [])

        self.servers_pool = {}
        self.unused_servers = []
        self._project_id = self.driver_opts.get('project_id', None)
        self.api = packet.Manager(auth_token=driver_opts['auth_token'])
        logger.info('Packet Driver initialized.')

    @staticmethod
    def supports_inspection() -> bool:
        return True

    @staticmethod
    def supports_capacity() -> bool:
        return True

    @property
    def capacity_per_machine(self) -> int:
        return self.driver_opts.get('capacity_per_machine', self.STARTING_CAPACITY[self.machine_size])

    def _map_to_server(self, server_object: packet.Device):
        """
        Map a Packet Device object of a server to a proper Server instance.

        :param server_object: the Packet device
        :type server_object: packet.Device
        :return: the tied server instance
        :rtype: Server
        """
        server_id = server_object.id
        verbose_name = server_object.hostname[len('flyngo-'):]
        public_ip = None
        for ip in server_object.ip_addresses:
            if ip['public'] and ip['address_family'] == 4:
                public_ip = ip['address']
        connection_parameters = {
            'hostname': public_ip,
            'username': server_object.user,
            'port': 22
        }
        return Server(server_id,
                      self,
                      connection_parameters,
                      capacity=0,  # We cannot assume these are not taken.
                      verbose_name=verbose_name)

    async def _fetch_project_id(self):
        assert 'project_name' in self.driver_opts
        projects = await self.loop.run_in_executor(
            None,
            self.api.list_projects
        )

        for project in projects:
            if project.name.lower() == self.driver_opts.get('project_name').lower():
                self._project_id = project.id
                return

    async def inspect(self):
        if 'project_id' not in self.driver_opts:
            await self._fetch_project_id()

        devices = await self.loop.run_in_executor(
            None,
            partial(
                self.api.list_devices,
                project_id=self._project_id,
                params={
                    'per_page': self.MAX_MACHINES
                }
            )
        )

        unused_tags = self.driver_opts['unused_tags']
        booted_tags = self.driver_opts['booted_tags']

        flyngo_servers = {server.id: server for server in devices if set(booted_tags) <= set(server.tags)}
        self.servers_pool = flyngo_servers

        used_servers = [self._map_to_server(server)
                        for server in flyngo_servers.values()
                        if set(server.tags).isdisjoint(set(unused_tags))]

        self.unused_servers = [self._map_to_server(server)
                               for server in flyngo_servers.values()
                               if set(unused_tags) <= set(server.tags)]

        logger.info('Inspection done, discovered: {0} used servers and {1} unused servers'
                    .format(len(used_servers), len(self.unused_servers)))

        return used_servers, self.unused_servers

    @property
    def machine_size(self):
        return self.driver_opts.get('machine_size',
                                    self.DEFAULT_MACHINE_SIZE)

    async def get_facility_capacity(self):
        capacity_data = await self.loop.run_in_executor(None,
                                                        self.api.call_api,
                                                        'capacity')

        return capacity_data['capacity'][self.driver_opts.get('facility', self.DEFAULT_FACILITY)][self.machine_size]

    def wait_for_server_to_boot(self, machine_data):
        machine_id = machine_data.id

        device = self.api.get_device(machine_id)
        while (
                    (not device.state == 'active')
                or
                    (not device.ip_addresses)):
            device = self.api.get_device(machine_id)
            logger.debug('Waiting for {0} to boot...'.format(device.hostname))
            time.sleep(1)

        return device

    def has_unused_servers(self) -> bool:
        return len(self.unused_servers) > 0

    def pick_a_free_server(self) -> Server:
        unused_server = self.unused_servers.pop(0)
        logger.info('Picking a free server: {}'.format(unused_server))
        unused_server.catch()
        # FIXME: update tags.
        return unused_server

    def recycle(self, server: Server) -> None:
        logger.info('Recycling server: {}'.format(server))
        self.unused_servers.append(server)
        # FIXME: update tags.

    async def update_capacity(self, server: Server, new_capacity: int):
        pass

    def _stop_and_destroy(self, server_id: str) -> None:
        device = self.api.get_device(server_id)

        device.power_off()
        device.delete()
        logger.info('{} machine deleted.'.format(device.hostname))

    async def destroy(self, server: Server) -> None:
        logger.info('Destroying machine ID: {}.'.format(server.id))
        await self.loop.run_in_executor(
            None,
            self._stop_and_destroy,
            server.id
        )

    async def fire_up_a_machine(self) -> Server:
        if 'project_id' not in self.driver_opts:
            await self._fetch_project_id()

        name = haikunator.haikunate(
            delimiter='-',
            token_length=0
        )
        start = time.time()
        hostname = 'flyngo-{}'.format(name)
        try:
            device = await self.loop.run_in_executor(  # type: packet.Device
                None,
                partial(
                    self.api
                        .create_device,
                    self._project_id,
                    hostname,
                    self.machine_size,
                    self.driver_opts.get('facility', self.DEFAULT_FACILITY),
                    self.driver_opts.get('os', self.DEFAULT_OPERATING_SYSTEM),
                    billing_cycle='hourly'
                )
            )
        except packet.baseapi.Error as e:
            raise RuntimeError('Packet returned an error while creating the machine: {}'.format(e))

        new_id = device.id

        self.servers_pool[new_id] = device

        # Wait for the machine to be ready.
        device = await self.loop.run_in_executor(
            None,
            self.wait_for_server_to_boot,
            device
        )

        public_ip = None
        for ip in device.ip_addresses:
            if ip['public'] and ip['address_family'] == 4:
                public_ip = ip['address']

        connection_parameters = {
            'hostname': public_ip,
            'port': 22,
            'username': device.user
        }

        elapsed = time.time() - start
        logger.info('Machine {} booted in {} seconds with ID: {}.'
                    .format(hostname, elapsed, new_id))

        return Server(new_id, self, connection_parameters,
                      verbose_name=name,
                      capacity=self.STARTING_CAPACITY[self.machine_size],
                      creation_speed=elapsed)
