from .abstract_artifact_handler import AbstractArtifactHandler
from .simple_artifact_handler import SimpleArtifactHandler


def create_artifact_handler(settings):
    storage = settings['storage'](settings['storage_opts'])
    return lambda: settings['artifact_handler'](storage, settings['artifact_opts'])
