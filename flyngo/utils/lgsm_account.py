import hashlib
from flyngo.servers.ssh_connection import SSHConnection
import os.path
from typing import Any

import logging

logger = logging.getLogger(__name__)

class LGSMAccount:
    """
        Wrapper under a SSH connection.
    """

    def __init__(self, ssh_access: SSHConnection):
        self.access = ssh_access

    async def exec(self, command, check=True):
        return await self.access.execute(command, check=check)

    async def install_lgsm(self):
        await self.exec('wget https://gameservermanagers.com/dl/csgoserver')
        await self.exec('chmod +x csgoserver')
        await self.exec('./csgoserver install')

    async def stop(self):
        return await self.exec('./csgoserver stop', check=False)

    async def restart(self):
        return await self.exec('./csgoserver restart')

    async def start(self):
        return await self.exec('./csgoserver start')

    async def update_lgsm(self):
        return await self.exec('./csgoserver update-functions')

    async def update_server_files(self):
        return await self.exec('./csgoserver update')

    async def validate_server_files(self):
        return await self.exec('./csgoserver validate')

    async def replace_basic_config(self, key: str, value: Any) -> None:
        # Match even commented options.
        command = "sed -i '/^#\? [ ]\?{key}=/c\\{key}=\"{value}\"' ./csgoserver"

        await self.exec(command
                        .format(key=key, value=value))

    async def replace_extra_config(self, key: str, value: Any) -> None:
        command = "sed -i '/^{key} /c\\{key} \"{value}\"' {path}"
        path = await self.advanced_config_path()

        not_changed = True
        attempts = 0
        while not_changed and attempts <= 5:
            sed_res = await self.exec(command
                                      .format(key=key, value=value.replace("'", "'\\''"),
                                              path=path))

            res = await self.exec('grep -i {key} {path}'.format(
                key=key,
                path=path
            ))
            if value in res.stdout:
                not_changed = False
            else:
                logger.info('Failed to modify the configuration, output: {} (content: {}, expected: {})'
                            .format(sed_res.stdout, res.stdout,
                                    value))
                attempts += 1

        if not_changed:
            raise RuntimeError('Failed to modify the configuration, permissions?')

    async def comment_option(self, key: str) -> None:
        # TODO: use backreferences.
        command = "sed -i '/^#\? [ ]\?{key}=/c\\# {key}=\"\"' ./csgoserver"

        await self.exec(command
                        .format(key=key))

    async def insert_extra_config(self, file_content: str):
        path = await self.advanced_config_path()
        sftp = await self.access.engage_sftp_subsystem()

        async with sftp.open(path, 'r') as remote_file:
            remote_content = await remote_file.read()

        # No need to do anything.
        if file_content == remote_content:
            return

        backup_path = path + '.bak'
        base_backup_file = os.path.basename(backup_path)
        max_id = 0
        list_dirs = await sftp.listdir(os.path.dirname(path))
        for name in list_dirs:
            if name.startswith(base_backup_file):
                if len(name) > len(base_backup_file):
                    try:
                        max_id = max(
                            int(name[len(base_backup_file) + 1:]) + 1,
                            max_id
                        )
                    except ValueError:
                        pass

        await sftp.copy(path, backup_path + '.' + str(max_id))
        hash_filename = hashlib.md5(file_content.encode('utf8')).hexdigest()
        localpath = '/tmp/{}'.format(hash_filename)
        with open(localpath, 'w') as f:
            f.write(file_content)

        await sftp.put(localpath, path)

    async def advanced_config_path(self, failures: int = 0, max_failures: int = 5) -> str:
        res = await self.exec('./csgoserver details')
        config_path = None
        for line in res.stdout.split('\n'):
            if line[5:].startswith('Config file:'):
                config_path = line[5:].split(' ')[-1][4:].strip()

        if not config_path and failures < max_failures:
            return await self.advanced_config_path(failures + 1)
        elif not config_path and failures >= max_failures:
            raise RuntimeError('Failed to discover configuration path after {} retries!'.format(failures))
            return None

        return config_path
