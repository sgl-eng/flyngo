from typing import Union, List, io
from .abstract_storage import AbstractStorage, StorageOptions
import os.path
import hashlib
import aiofiles


class LocalStorage(AbstractStorage):
    def __init__(self, storage_opts: StorageOptions = None):
        super().__init__(storage_opts)
        assert 'target_folder' in storage_opts

    async def save(self, file: Union[io, str]) -> None:
        if isinstance(file, str):
            filename = file
            file = await aiofiles.open(filename, 'rb')
            content = await file.read()
        else:
            content = await file.read()
            # Generate a hash based on the content.
            filename = hashlib.md5(content).hexdigest()

        target_path = os.path.join(
            self.storage_opts['target_folder'],
            filename
        )

        async with aiofiles.open(target_path, 'wb') as target:
            await target.write(content)

        file.close()

    async def batch_save(self, files: Union[List[io], List[str]]) -> None:
        for file in files:
            await self.save(file)
