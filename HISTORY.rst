.. :changelog:

History
-------

0.1.0 (2016-12-30)
++++++++++++++++++

* First release on PyPI.

0.1.1 (2017-03-10)
++++++++++++++++++

* Initial working release.
* Fix many bugs encountered during the first day of the tournament.
