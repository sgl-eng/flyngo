from typing import Optional, List, Dict
from flyngo.servers import Server, Instance
from functools import partial
import asyncio
import logging
import asyncssh
from flyngo.core import verify_configuration, resolve_configuration
from flyngo.artifacts import create_artifact_handler
import concurrent.futures
import itertools

logger = logging.getLogger(__name__)


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


default_cfg = {
    'artifact_handler': 'flyngo.artifacts.SimpleArtifactHandler',
    'storage': 'flyngo.storage.LocalStorage',
    'storage_opts': {
        'target_folder': 'flyngo_storage/'
    },
    'extra_settings': {}
}


class Runner:
    def __init__(self, configuration):
        updated_cfg = default_cfg
        updated_cfg.update(configuration)
        verify_configuration(updated_cfg)
        self.settings = resolve_configuration(updated_cfg)
        self.private_key = None
        self.public_key = None
        if 'extra_settings' in self.settings:
            self.public_key_path = self.settings['extra_settings'].get('public_key', None)
            self.private_key_path = self.settings['extra_settings'].get('private_key', None)
            if self.public_key_path:
                self.public_key = asyncssh.read_public_key(self.public_key_path)
            if self.private_key_path:
                self.private_key = asyncssh.read_private_key(self.private_key_path)

        for opts in ('driver_opts', 'provisionner_opts'):
            for opt in ('private_key', 'public_key', 'private_key_path', 'public_key_path'):
                self.settings[opts][opt] = getattr(self, opt, None)

        self.driver = self.settings['driver'](self.settings['driver_opts'])
        self.artifact_builder = create_artifact_handler(self.settings)
        self.provisionner = self.create_provisionner()

    def create_provisionner(self):
        return self.settings['provisionner'](self.settings['provisionner_opts'])

    async def provision_server(self,
                               server: Server,
                               servers_params: List[Dict[str, str]],
                               single: bool = False,
                               fresh: bool = False) -> List[Instance]:
        server.artifact_handler = self.artifact_builder()
        logger.info('Provisionner ready.')

        game_opts = self.settings['game_opts']
        extra_parameters = []
        for server_params in servers_params:
            params = game_opts.copy()
            params.update(server_params)
            extra_parameters.append(params)

        logger.info('Running provisionner now.')
        instances = await self.provisionner.provision(server,
                                                      extra_parameters=extra_parameters,
                                                      single=single,
                                                      fresh=fresh)

        logger.info('Provisionning finished: {}'.format(str(server)))
        await server.add_instances(instances)
        logger.info('Server ready: {}'.format(str(server)))

        return instances

    async def get_free_server(self, capacity: int = 1) -> Server:
        if not self.driver.supports_capacity():
            return await self.driver.fire_up_a_machine()

        server = await self.driver.get_server_with_enough_capacity(capacity)

        if not server:
            return await self.driver.fire_up_a_machine()

        return server

    async def get_server(self, capacity: int = 1) -> Server:
        if self.driver.has_unused_servers():
            server_instance = self.driver.pick_a_free_server()
        else:
            server_instance = await self.get_free_server(capacity)

        return server_instance

    async def start_server(self,
                           hostname: str,
                           password: Optional[str] = None,
                           force_deploy: bool = False) -> Instance:
        logger.info('Starting a new server.')
        if self.driver.has_unused_servers() and not force_deploy:
            server_instance = self.driver.pick_a_free_server()
            logger.info('Got a free server: {}'.format(server_instance.id))
        else:
            server_instance = await self.driver.fire_up_a_machine()
            logger.info('Fired up a new machine: {}'.format(server_instance.id))

        instances = await self.provision_server(
            server_instance,
            [{'hostname': hostname, 'password': password}],
            single=True,
            fresh=False)

        return instances[0]

    async def start_servers(self,
                            servers_params: List[Dict[str, str]],
                            capacity_per_machine: Optional[int] = None) -> List[Instance]:
        # FIXME: big refactor necessary to support pool reuse through multiple servers boot.
        params = servers_params.copy()
        capacity = capacity_per_machine or self.driver.capacity_per_machine
        pending_work = set()
        instances = []
        chunk_params = []

        for param in chunks(params, capacity):
            pending_work.add(self.driver.fire_up_a_machine())
            chunk_params.append(param)

        while pending_work:
            try:
                done, pending_work = await asyncio.wait(pending_work, return_when=concurrent.futures.FIRST_COMPLETED)
                for server_task in filter(lambda task: isinstance(task.result(), Server), done):
                    if server_task.exception():
                        logger.error('Exception occurred during provisionning: ', str(server_task.exception()))
                        pending_work.add(
                            self.driver.fire_up_a_machine()
                        )
                        continue
                    server = server_task.result()
                    param = chunk_params.pop(0)
                    pending_work.add(
                        self.provision_server(server, param)
                    )
                for provision_task in filter(lambda task: isinstance(task.result(), list), done):
                    chunk_of_instances = provision_task.result()
                    instances.extend(chunk_of_instances)
            except RuntimeError as e:
                logger.error('One or multiple errors has happened during the server processing', str(e))

        return instances

    async def inspect_for_servers(self):
        if not self.driver.supports_inspection():
            raise RuntimeError('Driver does not support inspection!')

        return await self.driver.inspect()
